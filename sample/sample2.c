#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    char    *test;
    size_t  len = 7;

    test = (char *)malloc(sizeof(char) * len);
    test = memcpy(test, "Youssof", len);
    printf("Hello %s !\n", test);
    return (0);
}
