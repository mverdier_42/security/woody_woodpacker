#include <stdio.h>
#include "woody.h"

int		main(int ac, char **av) {
	t_key	key;
	char	*filename;

	(void)ac;
	filename = get_key(av, &key);
	if (filename != NULL)
		pack(filename, &key);
	free(key.key);
	return (0);
}
