#include "woody.h"

int		init_target(t_target *target, char *filename, uint32_t add_size)
{
	t_target	src;

	src.fd = init_file(filename, &src.data, &src.size);
	if (src.fd < 0 || !src.data)
		return (0);
	if (!check_target(&src)) {
		close_unmap(src.fd, &src.data, src.size);
		return (0);
	}
	target->size = src.size + add_size;
	if ((target->data = malloc(target->size)) == NULL) {
		perror("malloc error");
		close_unmap(src.fd, &src.data, src.size);
		return (0);
	}
	memcpy(target->data, src.data, src.size - 1);
	target->is_64 = src.is_64;
	target->swap = src.swap;
	target->hdr = (Elf64_Ehdr*)target->data;
	close_unmap(src.fd, &src.data, src.size);
	return (1);
}
