#include "woody.h"

Elf64_Word get_flags_segment(t_target *target, Elf64_Addr section_addr, Elf64_Off section_size)
{
	if (target->hdr->e_phoff + (target->hdr->e_phnum * sizeof(Elf64_Phdr)) > target->size)
		exit_corrupted(target);

	Elf64_Phdr *phdr = (Elf64_Phdr *)(target->data + target->hdr->e_phoff);

	for (int i = 0; i < target->hdr->e_phnum; i++)
	{
		if (phdr[i].p_vaddr <= section_addr && (phdr[i].p_vaddr + phdr[i].p_filesz) >= (section_addr + section_size))
		{
			return phdr[i].p_flags;
		}
	}
	return (0);
}
