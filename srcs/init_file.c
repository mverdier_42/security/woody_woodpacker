#include "woody.h"

int		init_file(char *filename, void **ptr, uint32_t *size) {
	int			fd;
	struct stat	buf;

	if ((fd = open(filename, O_APPEND | O_RDONLY)) < 0)
		perror("open error");
	else if (fstat(fd, &buf) < 0)
		perror("fstat error");
	else if ((buf.st_mode & S_IFMT) != S_IFREG && (buf.st_mode & S_IFMT) != S_IFLNK
		&& (buf.st_mode & S_IFMT) != S_IFSOCK)
		dprintf(2, "Error: File is not valid\n");
	else if (buf.st_size <= 0)
		dprintf(2, "Error: File is empty\n");
	else if ((buf.st_uid == getuid() && !(buf.st_mode & S_IXUSR))
		|| (buf.st_gid == getgid() && !(buf.st_mode & S_IXGRP))
		|| !(buf.st_mode & S_IXOTH))
		dprintf(2, "Error: user do not have execute permissions on target\n");
	else if ((*ptr = mmap(0, buf.st_size, PROT_READ,
			MAP_PRIVATE, fd, 0)) == MAP_FAILED)
		perror("mmap error");
	else {
		*size = buf.st_size;
		return (fd);
	}
	return (-1);
}
