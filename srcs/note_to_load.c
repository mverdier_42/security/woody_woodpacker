#include "woody.h"

//	========================== PT_NOTE TO PT_LOAD ========================== //
/*
	Cette fonction permet de convertir le segment PT_NOTE en PT_LOAD pour pouvoir
	Injecter du code dedans et l'executer avant le code du binaire.

	target: le nouveau binaire
	payload_size: la taille du parasite que l'on veut injecter
	base: l'addresse à laquelle on va injecter le parasite

	On parcourt les segments pour faire 2 choses, la première est de rajouter
	le droit d'écriture (PF_W) au segment text pour pouvoir le crypter.
	Ensuite on récupère l'index du segment PT_NOTE (s'il y en a)pour
	patcher le tout (ne pas oublier de décaller les segments suivant pour éviter
	l'overlap). Enfin, on retourne l'offset à partir duquel on peut écrire,
	et l'addresse qui correspond.z*/

uint64_t	ptnote_to_ptload(t_target *target, uint32_t payload_size, uint64_t *base)
{
	if (target->hdr->e_phoff + (target->hdr->e_phnum * sizeof(Elf64_Phdr)) > target->size)
		exit_corrupted(target);

	Elf64_Phdr *phdr = (Elf64_Phdr *)(target->data + target->hdr->e_phoff);
	int			phnum = target->hdr->e_phnum, note_index = -1;

	for (int i = 0; i < phnum; i++)
	{
		if (phdr[i].p_type == PT_LOAD && phdr[i].p_flags == 0x5)
			phdr[i].p_flags |= PF_W;
		if (phdr[i].p_type == PT_NOTE)
			note_index = i;
	}
	if (note_index == -1) {
		for (int i = 0; i < phnum; i++) {
			if (phdr[i].p_type == PT_LOAD && phdr[i].p_flags == 0x7 && phdr[i].p_vaddr >= 0xc000000)
				note_index = i;
		}
		if (note_index == -1) {
			printf("No space found or can be create to inject payload.\n");
			free(target->data);
			free(target->key->key);
			exit(0);
		}
		phdr[note_index].p_filesz += payload_size;
		phdr[note_index].p_memsz += payload_size;
		*base = phdr[note_index].p_vaddr + phdr[note_index].p_filesz - payload_size;
		return (phdr[note_index].p_offset + phdr[note_index].p_filesz - payload_size);
	} else {
		phdr[note_index].p_type = PT_LOAD;
		phdr[note_index].p_flags |= (PF_W | PF_X);
		phdr[note_index].p_filesz = payload_size;
		phdr[note_index].p_memsz = payload_size;
		phdr[note_index].p_offset = target->size - payload_size;
		phdr[note_index].p_vaddr = 0xc000000 + (target->size - payload_size);
		*base = phdr[note_index].p_vaddr;
		return (phdr[note_index].p_offset);
	}
}
