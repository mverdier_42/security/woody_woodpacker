#include "woody.h"

static void	usage(char *av0)
{
	dprintf(2, "Usage: %s [-s <string>] <file>\n       %s [-p] <file>\n", av0, av0);
	exit(1);
}

static void	read_key(t_key *key)
{
	char	buff[128];
	int		len;
	int		i;

	i = 0;
	while ((len = read(0, buff, 128)) != 0) {
		if (i > 0 || len < 1) {
			dprintf(2, "Key must be [1 - 128] bytes long\n");
			exit(1);
		}
		key->key = strdup(buff);
		key->size = len;
		i++;
	}
	if (len < 0 || (len == 0 && !key->key)) {
		dprintf(2, "Key must be [1 - 128] bytes long\n");
		free(key->key);
		exit(1);
	}
}

static void	gen_key(t_key *key)
{
	char	buff[128];
	int		len;
	int		fd;

	if ((fd = open("/dev/urandom", O_RDONLY)) < 0) {
		perror("open error");
		exit(1);
	}
	if (read(fd, buff, 1) != 1) {
		perror("read error");
		exit(1);
	}
	len = ((uint8_t)buff[0] % 127) + 1;
	if (read(fd, buff, len) != len) {
		perror("read error");
		exit(1);
	}
	key->key = strdup(buff);
	key->size = len;
}

char		*get_key(char **av, t_key *key)
{
	int		flag = FLAG_NONE;
	char	*file = NULL;
	char	*av0 = *av;

	key->key = NULL;
	while (*(++av) != NULL) {
		if (flag == FLAG_S && !key->key) {
			key->size = strlen(*av);
			if (key->size < 1 || key->size > 128) {
				dprintf(2, "Key must be [1 - 128] bytes long\n");
				exit(1);
			}
			key->key = strdup(*av);
		}
		else if (!strcmp(*av, "-s") && flag == FLAG_NONE)
			flag = FLAG_S;
		else if (!strcmp(*av, "-p") && flag == FLAG_NONE)
			flag = FLAG_P;
		else if (!file)
			file = *av;
		else
			usage(av0);
	}
	if (!file)
		usage(av0);
	if (flag == FLAG_P) {
		read_key(key);
	}
	if (flag == FLAG_NONE)
		gen_key(key);
	return (file);
}
