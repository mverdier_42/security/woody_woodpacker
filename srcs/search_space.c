#include "woody.h"

// ============================== SEARCH SPACE ============================== //
/*
	Cette fonction permet de trouver une codecave s'il y en une (c'est à dire
	un emplacement de libre entre deux segments).

	target: le nouveau binaire
	payload_size: la taille du parasite
	base: l'addresse à laquelle on va injecter le parasite

	On parcourt les segments à la recherche du PT_LOAD qui a les droits
	d'executions (pour pouvoir lancer notre parasite).
	Si on en trouve un, on lui donne le droit d'écriture pour écrire dedans et
	crypter le reste. On patch le segment pour préparer l'ajout du parasite.
	Enfin on renvoit l'addresse et l'offset de l'endroit où l'on peut injecter
	notre parasite
*/

uint64_t	search_space(t_target *target, uint64_t payload_size, uint64_t *base)
{
	int seg_nbr = target->hdr->e_phnum;
	Elf64_Phdr *seg, *old_seg;
	uint64_t old_seg_end;

	seg = (Elf64_Phdr *)((unsigned char *)target->hdr + (unsigned int)target->hdr->e_phoff);
	if ((void*)seg + sizeof(*seg) > target->data + target->size)
		exit_corrupted(target);
	for (int i = 0; i < seg_nbr; i++)
	{
		old_seg = seg;
		old_seg_end = ALIGN(seg->p_offset + seg->p_filesz, 16);
		seg = (Elf64_Phdr *)((unsigned char *)seg + (unsigned int)target->hdr->e_phentsize);
		if ((void*)seg + sizeof(*seg) > target->data + target->size)
			exit_corrupted(target);
		if (old_seg->p_type == PT_LOAD && (old_seg->p_flags == 0x5 || old_seg->p_flags == 0x7)
			&& (int)(seg->p_offset - old_seg_end) >= (int)payload_size)
		{
			*base = ALIGN(old_seg->p_vaddr + old_seg->p_filesz, 16);
			old_seg->p_flags |= PF_W;
			old_seg->p_memsz += payload_size + (*base - (old_seg->p_vaddr + old_seg->p_filesz));
			old_seg->p_filesz += payload_size + (old_seg_end - (old_seg->p_offset + old_seg->p_filesz));
			return (old_seg_end);		// Comment this to force segment note injection
			// return (0);				// Uncomment this to force segment note injection
		}
	}
	return (0);
}
