#include "woody.h"

static void	print_key(t_key *key)
{
	printf("key(%d): [", key->size);
	for (uint32_t i = 0; i < key->size; i++) {
		printf("%.2x", *(uint8_t*)(key->key + i));
	}
	printf("]\n");
}

void		pack(char *filename, t_key *key) {
	t_target	target;
	t_payload	payload;
	uint64_t	cave, base, text_poff, text_vaddr, text_size;
	int			old_ep, text_voff;

	target.data = NULL;
	target.key = key;
	if (!init_target(&target, filename, 0))
		return ;
	payload.data = &rc4_decrypt;
	payload.size = &end - &rc4_decrypt + 1;
	if (!(cave = search_space(&target, payload.size, &base))) {
		close_unmap(target.fd, &target.data, target.size);
		if (!init_target(&target, filename, payload.size))
			return ;
		// printf("no codecave found, trying PT_NOTE to PT_LOAD injection\n");
		cave = ptnote_to_ptload(&target, payload.size, &base);
	}
	text_poff = get_text_off(&target, &text_size, &text_vaddr);
	if (!text_poff || (__uint128_t)text_poff + (__uint128_t)text_size > target.size)
		exit_corrupted(&target);
	text_voff = base - text_vaddr;
	old_ep = base - target.hdr->e_entry;
	target.hdr->e_entry = base;

	if (target.data + text_poff + text_size > target.data + target.size)
		exit_corrupted(&target);
	rc4_encrypt((uint64_t)target.data + text_poff, text_size, (uint8_t*)key->key, key->size);

	if (target.data + cave + payload.size > target.data + target.size)
		exit_corrupted(&target);
	memcpy(target.data + cave, payload.data, payload.size);
	memcpy(target.data + cave + (&decrypt - &rc4_decrypt) - 9, &text_voff, sizeof(text_voff));		// start_addr offset to decrypt
	memcpy(target.data + cave + (&decrypt - &rc4_decrypt) - 4, &text_size, sizeof(uint32_t));		// size to decrypt
	memcpy(target.data + cave + (&jump - &rc4_decrypt) - 4, &old_ep, sizeof(old_ep));				// offset to jump back to original entry point
	memcpy(target.data + cave + (&datas - &rc4_decrypt), key_tab_cpy, 256);							// key_tab for decryption

	write_woody(&target);
	print_key(key);
	free(target.data);
}
