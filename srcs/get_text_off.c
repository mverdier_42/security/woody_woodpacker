#include "woody.h"

Elf64_Off	get_text_off(t_target *target, uint64_t *text_size, uint64_t *text_vaddr)
{
	if ((__uint128_t)target->hdr->e_shoff + sizeof(Elf64_Shdr) > target->size)
		exit_corrupted(target);
	if ((__uint128_t)target->hdr->e_shoff + (target->hdr->e_shstrndx * sizeof(Elf64_Shdr)) > target->size)
		exit_corrupted(target);

	Elf64_Shdr	*shdr = (Elf64_Shdr *)(target->data + target->hdr->e_shoff);
	Elf64_Shdr	shstrsect = shdr[target->hdr->e_shstrndx];

	if (shstrsect.sh_offset > target->size)
		exit_corrupted(target);

	char		*shstrtable = target->data + shstrsect.sh_offset;

	for (int i = 0; i < target->hdr->e_shnum; i++)
	{
		if ((__uint128_t)target->hdr->e_shoff + (i * sizeof(Elf64_Shdr)) > target->size)
			exit_corrupted(target);
		if ((__uint128_t)shstrsect.sh_offset + shdr[i].sh_name + 6 > target->size)
			exit_corrupted(target);
		if (strcmp(shstrtable + shdr[i].sh_name, ".text") == 0)
		{
			if (get_flags_segment(target, shdr[i].sh_addr, shdr[i].sh_size) == 7)
			{
				if (shdr[i].sh_offset > shdr[i].sh_addr)
					exit_corrupted(target);
				*text_vaddr = shdr[i].sh_addr;
				*text_size = shdr[i].sh_size;
				return (shdr[i].sh_offset);
			}
		}
	}
	exit_corrupted(target);
	return (0);
}
