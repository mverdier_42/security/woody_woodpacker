#include "woody.h"

void	write_woody(t_target *target)
{
	if ((target->fd = open("woody", O_CREAT | O_TRUNC | O_RDWR,
			S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)) < 0) {
		perror("open error");
		free(target->data);
		free(target->key->key);
		exit(1);
	}
	if (write(target->fd, target->data, target->size) < 0)
	{
		perror("write error");
		close(target->fd);
		free(target->data);
		free(target->key->key);
		exit(1);
	}
	close(target->fd);
}
