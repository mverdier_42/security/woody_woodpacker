#include "woody.h"

int		check_target(t_target *target)
{
	if (target->data + sizeof(Elf64_Ehdr) > target->data + target->size) {
		dprintf(2, "Error: file is not an elf executable\n");
		return (0);
	}
	target->hdr = (Elf64_Ehdr*)target->data;
	if (target->hdr->e_ident[EI_MAG0] != '\x7f' || target->hdr->e_ident[EI_MAG1] != 'E'
		|| target->hdr->e_ident[EI_MAG2] != 'L' || target->hdr->e_ident[EI_MAG3] != 'F') {
		dprintf(2, "Error: file is not elf executable\n");
		return (0);
	}
	if (target->hdr->e_type != ET_EXEC && target->hdr->e_type != ET_DYN) {
		dprintf(2, "Error: file type not supported\n");
		return (0);
	}
	target->is_64 = target->hdr->e_ident[EI_CLASS] == ELFCLASS64 ? true : false;
	target->swap = target->hdr->e_ident[EI_DATA] == ELFDATA2MSB ? true : false;
	if (!target->is_64 || target->swap) {
		dprintf(2, "Error: architecture not supported\n");
		return (0);
	}
	return (1);
}
