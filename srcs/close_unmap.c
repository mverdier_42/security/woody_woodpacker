#include "woody.h"

void	close_unmap(int fd, void **d, uint32_t size)
{
	if (d != NULL && *d != NULL && size > 0)
		munmap(*d, size);
	if (fd >= 0)
		close (fd);
}
