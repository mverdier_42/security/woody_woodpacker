section .text
	global rc4_decrypt
	global decrypt
	global jump
	global datas
	global end

rc4_decrypt:
	;save stack state
	push rax
	push rbx
	push rcx
	push rdx
	push rbp
	push rsp
	push rsi
	push rdi
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15

	lea r8, [rel key_tab_dec]		; key_tab
	xor r9, r9						; i
	xor r10, r10					; j
	xor r11, r11					; tmp_i
	xor r12, r12					; tmp_j
	xor r13, r13					; k
	xor r14, r14					; K
	xor r15, r15					; l
	xor rdi, rdi
	xor rsi, rsi
	lea rdi, [rel rc4_decrypt]
	sub rdi, 0x22222222				; start_addr offset to decrypt
	mov rsi, 0x33333333				; size to decrypt from start_addr

decrypt:
	cmp r15, rsi					; while (l < size)
	jge woody
	add r9b, 1						; i = (i + 1) % 256
	add r10b, [r8 + r9]				; j = j + key_tab[i]
	mov r11b, [r8 + r9]				; tmp_i = key_tab[i]
	mov r12b, [r8 + r10]			; tmp_j = key_tab[j]
	mov [r8 + r10], r11b			; key_tab[j] = tmp_i
	mov [r8 + r9], r12b				; key_tab[i] = tmp_j
	mov r13b, [r8 + r9]				; k = key_tab[i]
	add r13b, [r8 + r10]			; k = k + key_tab[j]
	mov r14b, [r8 + r13]			; K = key_tab[k]
	xor [rdi + r15], r14b			; byte to encrpt ^ K
	inc r15							; l++
	jmp decrypt

woody:
	mov rax, 1						; [1] - sys_write
	mov rdi, 1						; 0 = stdin / 1 = stdout / 2 = stderr
	lea rsi, [rel msg]				; pointer(mem address) to msg (*char[])
	mov rdx, end - msg				; msg size
	syscall							; calls the function stored in rax

	;restore stack state
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rdi
	pop rsi
	pop rsp
	pop rbp
	pop rdx
	pop rcx
	pop rbx
	pop rax

	;set rax with the original entry point and jump to it
	lea rax, [rel rc4_decrypt]
	sub rax, 0x11111111				; offset of old_ep from payload

jump:
	jmp rax

datas:
	key_tab_dec resb 256
	msg db '....WOODY....',0x0a
	end db 0x0
