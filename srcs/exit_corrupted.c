#include "woody.h"

void	exit_corrupted(t_target *target)
{
	if (target->data)
		free(target->data);
	if (target->key->key)
		free(target->key->key);
	dprintf(2, "Error: target is corrupted\n");
	exit(1);
}
