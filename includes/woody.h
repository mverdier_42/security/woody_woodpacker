#ifndef	WOODY_H
#define	WOODY_H

#include <stdbool.h>
#include <elf.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <inttypes.h>

#define PAGE_SIZE getpagesize()
#define ALIGN(pyl, x) ((pyl + (x - 1)) & ~(x - 1))

#define FLAG_NONE 0		// random key
#define FLAG_S 1		// key in argv
#define FLAG_P 2		// key on stdin

// payload64.s labels
extern uint8_t	rc4_decrypt;
extern uint8_t	decrypt;
extern uint8_t	jump;
extern uint8_t	datas;
extern uint8_t	end;

// payload64.s datas
extern uint8_t	key_tab_dec[256];

// rc4_encrypt.s datas
extern uint8_t	key_tab_cpy[256];

typedef struct	s_key
{
	char		*key;
	uint32_t	size;
}				t_key;

typedef struct	s_target
{
	void		*data;
	uint32_t	size;
	int			fd;
	bool		is_64;
	bool		swap;
	Elf64_Ehdr	*hdr;
	uint64_t	text_addr;
	t_key		*key;
}				t_target;

typedef struct	s_payload
{
	void		*data;
	uint32_t	size;
	uint64_t	text_start;
}				t_payload;

// Functions in get_key.c
char			*get_key(char **av, t_key *key);

// Functions in pack.c
void			pack(char *filename, t_key *key);

// Functions in init_file.c
int				init_file(char *filename, void **ptr, uint32_t *len);

// Functions in init_target.c
int				init_target(t_target *target, char *filename, uint32_t add_size);

// Functions in check_target.c
int				check_target(t_target *target);

// Functions in search_space.c
uint64_t		search_space(t_target *target, uint64_t payload_size, uint64_t *base);

// Functions in close_unmap.c
void			close_unmap(int fd, void **d, uint32_t size);

// Functions in exit_corrupted.c
void			exit_corrupted(t_target *target);

// Functions in rc4_encrypt.s
void			rc4_encrypt(uint64_t offset, uint64_t size, uint8_t *key, uint64_t key_size);

// Functions in note_to_load.c
uint64_t		ptnote_to_ptload(t_target *target, uint32_t payload_size, uint64_t *base);

// Functions in get_text_off.c
Elf64_Off		get_text_off(t_target *target, uint64_t *text_size, uint64_t *text_vaddr);

// Functions in write_woody.c
void			write_woody(t_target *target);

//Functions in get_flags_segment.c
Elf64_Word		get_flags_segment(t_target *target, Elf64_Addr section_addr, Elf64_Off section_size);

#endif
