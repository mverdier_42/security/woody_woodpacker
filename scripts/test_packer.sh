#!/bin/sh

# sh test_packer.sh [test file(s)] (ex: sh test_packer.sh /bin/*)

echo -n "" > res
echo -n "" > err

for file in $@ # /bin/* /sbin/* /usr/bin/* /usr/sbin/* /usr/lib/*
do
	# echo "diff on $file"
	echo "$file: " >> res
	echo "$file: " >> err
	./woody_woodpacker $file 1>>res 2>>err
done
