#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>

int		main(int ac, char **av)
{
	int			fd, size, tmp;
	void		*target;
	struct stat	buf;

	if (ac != 2)
	{
		printf("Usage: %s <target binary>\n", av[0]);
		return (0);
	}
	if ((fd = open(av[1], O_RDONLY)) < 0) {
		perror("target open error");
		return (1);
	}
	if (fstat(fd, &buf) < 0) {
		perror("fstat error");
		close(fd);
		return (1);
	}
	size = buf.st_size;
	if ((target = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
		perror("mmap error");
		close(fd);
		return (1);
	}
	close(fd);
	system("rm -rf packer_res packer_err woody_res woody_err tmp_res tmp_err tmp");
	for (int i = 0; i < size; i++) {
		if ((tmp = open("tmp", O_CREAT | O_APPEND | O_WRONLY, S_IRWXU | S_IRWXG | S_IRWXO)) < 0) {
			perror("tmp open error");
			munmap(target, size);
			return (1);
		}
		write(tmp, target + i, 1);
		close(tmp);
		system("rm -rf woody");
		system("./woody_woodpacker tmp 1>>packer_res 2>>packer_err");
	}
	munmap(target, size);
}
