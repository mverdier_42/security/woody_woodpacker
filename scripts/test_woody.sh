#!/bin/sh

# sh test_packer.sh [test file(s)] 2>log (ex: sh test_packer.sh /bin/* 2>log)

echo -n "" > res_woody
echo -n "" > err_woody

for file in $@ # /bin/* /sbin/* /usr/bin/* /usr/sbin/* /usr/lib/*
do
	if [ "$file" = "/bin/usbreset" ]
	then
		continue
	fi
	echo "---------- $file: ----------" 1>&2
	echo "$file: " >> res_woody
	echo "$file: " >> err_woody
	rm -rf woody
	./woody_woodpacker $file > /dev/null 2>/dev/null
	echo -n "" | ./woody --version 1>>res_woody 2>>err_woody
done
