# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2019/11/21 15:22:46 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

MAKEFLAGS += --silent	#For silent on linux

# Colors.

ORANGE =	\033[1;33m	#It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#
# list all sources, objects and includes files for 'woody_woodpacker'.

NAME = 		woody_woodpacker

SRCDIR =	./srcs

OBJDIR =	./objs

INCDIR =	./includes

SRC =		main.c pack.c search_space.c close_unmap.c init_file.c		\
			check_target.c get_key.c note_to_load.c exit_corrupted.c	\
			init_target.c get_text_off.c write_woody.c get_flags_segment.c

ASM_SRC =	payload64.s rc4_encrypt.s

INC =		woody.h

SRCS =		$(SRC:%=$(SRCDIR)/%)

ASM_SRCS =	$(ASM_SRC:%=$(SRCDIR)/%)

OBJS =		$(SRC:%.c=$(OBJDIR)/%.o)

ASM_OBJS =	$(ASM_SRC:%.s=$(OBJDIR)/%.o)

INCS =		$(INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all compilation variables.

CC =		gcc -no-pie

CFLAGS =	-Wall			\
			-Wextra			\
			-Werror			\

INCFLAGS =	-I $(INCDIR)

FLAGS =		$(CFLAGS)		\
			$(INCFLAGS)

#------------------------------------------------------------------------------#
# List all rules used to make libft.a

all:
	@$(MAKE) $(NAME)

$(NAME): $(OBJS) $(ASM_OBJS)
	@$(MAKE) printname
	@printf "%-15s%s\n" Linking $@
	@$(CC) $(CFLAGS) $^ -o $@
	@printf "$(GREEN)"
	@echo "Compilation done !"
	@printf "$(RES)"

#./payloads/helloworld64: $(P_OBJDIR)/helloworld64.o
#	@$(MAKE) printname
#	@printf "%-15s%s\n" Linking $@
#	@ld -o ./payloads/helloworld64 $(P_OBJDIR)/helloworld64.o

$(OBJS): $(INCS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(OBJDIR)/%.o: $(SRCDIR)/%.s
	@mkdir -p $(OBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@nasm -f elf64 -o $@ $<

printname:
	@printf "$(ORANGE)"
	@printf "[%-15s " "$(NAME)]"
	@printf "$(RES)"

clean:
	@$(MAKE) printname
	@echo Suppressing obj files
	@printf "$(RED)"
	rm -rf $(OBJS) $(P_OBJDIR)
	@rm -rf $(OBJDIR)
	@printf "$(RES)"

fclean: clean
	@$(MAKE) printname
	@echo Suppressing $(NAME)
	@printf "$(RED)"
	rm -rf $(NAME)
	rm -rf ./payloads/helloworld64
	@printf "$(RES)"

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

git:
	@$(MAKE) printname
	@echo Adding files to git repository
	@printf "$(GREEN)"
	git add $(SRCS) $(INCS) Makefile
	@printf "$(RES)"
	@git status

.PHONY: all clean re fclean git printname
